import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import xgboost as xgb
from dash.dependencies import Input, Output
from sklearn import metrics
from sklearn.model_selection import train_test_split

import data_frames as dfs
from dash_app import app
from simple_file_for_import import graph


# Показ графика
@app.callback(
    Output('graphs', 'children'),
    [Input('create-model', 'n_clicks'),
     Input('dt-predictors-imp', 'derived_viewport_selected_rows'),
     Input('dt-predictors-imp', 'derived_viewport_data'),
     ]
)
def show_graph(button_click, selected_rows, data):
    if button_click is not None:

        # поиск выбранных колонок
        selected_data = []
        for i in selected_rows:
            selected_data.append(data[i])
        current_data_frame = pd.DataFrame(selected_data)
        selected_features = list(current_data_frame["ИмяКолонки"].values)

        objective_function = "nedads_flg"
        X = dfs.main_df[selected_features]
        y = dfs.main_df[objective_function]
        # print(X, y)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=11)
        N_train, _ = X_train.shape
        N_test, _ = X_test.shape
        gbr = xgb.XGBClassifier(learning_rate=1.5, max_depth=3, n_estimators=50, min_child_weight=10,
                                random_state=1)
        gbr.fit(X_train, y_train)
        # model1 = LogisticRegression()
        # model1.fit(X_train, y_train)
        # err_train = np.mean(y_train != model1.predict(X_train))
        # err_test = np.mean(y_test != model1.predict(X_test))
        probs = gbr.predict_proba(X_test)
        preds = probs[:, 1]
        fpr, tpr, threshold = metrics.roc_curve(y_test, preds)
        # roc_auc = metrics.roc_curve(y_test, probs)

        xaxis_type = 'Linear'
        yaxis_type = 'Linear'
        xaxis_column_name = 'True Positive Rate'
        yaxis_column_name = 'False Positive Rate'
        xaxis_column = fpr
        yaxis_column = tpr
        text_marks = 'AUC = %0.2f'  # % metrics.roc_curve(y_test, probs)#roc_auc

        return [
            dcc.Graph(id='gr1',
                      figure=graph(xaxis_column, yaxis_column, text_marks, xaxis_column_name, xaxis_type,
                                   yaxis_column_name, yaxis_type)),
            # dcc.Graph(id='gr2', figure={}),
            # dcc.Graph(id='gr3', figure={}),

        ]


@app.callback(
    Output('div-result-model', 'children'),
    [Input('create-model', 'n_clicks')]
)
def out_result(buton_clicks):
    f_measure = 1 / ((dfs.res_for_graph_conf_result[1]) + (dfs.res_for_graph_conf_result[2]))

    if buton_clicks is not None:
        return html.Div(id='res1', children=[
            html.Hr(),  # conf_matrix
            html.Table(
                [html.Tr([html.Th(['Confusion matrix'])])] +
                [html.Tr([html.Th(col) for col in ['False_pos', 'True_pos', 'True_neg', 'False_neg']])] +
                [html.Tr([
                    html.Td(str(col)) for col in dfs.res_for_graph_conf_matrix
                ]) for i in range(1)]
            ),
            html.Table(
                [html.Tr(
                    [html.Th(col) for col in ['Accuracy', 'Precision', 'Recall', 'Specificity', 'F-measure']])] +
                [html.Tr([
                    html.Td(str(col)) for col in (dfs.res_for_graph_conf_result + [f_measure])
                ]) for i in range(1)]
            ),
            html.Hr(),
        ]),


@app.callback(
    Output('output-objective-function', 'children'),
    [Input('objective-function', 'value'),
     Input('objective-function-button', 'n_clicks')]
)
def output_objective_function_field(objective_function_values, button_clicks):
    if objective_function_values is not None:
        return ['Цель моделирования: ' + str(dfs.target_objective_function[-1])]


# Показ и скрытие полей

@app.callback(
    Output('objective-function-button', 'style'),
    [Input('objective-function', 'value'),
     Input('objective-function-button', 'n_clicks')]
)
def show_hide_objective_function_button(objective_function_values, button_click):
    objective_function_button = 'none'
    if objective_function_values != 'None':
        target_objective_function_decr = list(dfs.for_choices_data_df["decr"].values)
        dfs.target_objective_function.extend(target_objective_function_decr)
        objective_function_button = 'inline'
        if button_click is not None:
            if button_click % 2 == 0:
                objective_function_button = 'inline'
            else:
                objective_function_button = 'none'
    return {'display': objective_function_button}


@app.callback(
    Output('div-objective-function', 'style'),
    [Input('objective-function', 'value'),
     Input('objective-function-button', 'n_clicks')]
)
def hide_section_after_selected_objective_function(objective_function_values, button_click):
    objective_function_section = 'inline'
    if button_click is not None:
        if button_click % 2 == 1:
            objective_function_section = 'none'
        else:
            objective_function_section = 'inline'
    return {'display': objective_function_section}


@app.callback(
    Output('choose1-objective-function', 'style'),
    [Input('objective-function', 'value'),
     Input('objective-function-button', 'n_clicks')]
)
def show_body_after_selected_objective_function(objective_function_values, button_click):
    objective_function_button = 'none'
    if objective_function_values != 'None':
        if button_click is not None:
            if button_click % 2 == 1:
                objective_function_button = 'inline'
            else:
                objective_function_button = 'none'
    return {'display': objective_function_button}
