from dash_app import app
import index_html
import table_predictors
import some_callbacks

if __name__ == '__main__':
    app.run_server(debug=False, port=8051)
