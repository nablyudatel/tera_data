from sklearn.model_selection import train_test_split
import numpy as np
import xgboost as xgb


def confusion(data, objective_function, model):
    df_pos = data[data[objective_function] == 1]
    df_neg = data[data[objective_function] == 0]
    # np.mean -> count/len/shape
    #print(data.shape) = (12356, 37)
    true_pos = df_pos[df_pos[objective_function] == model.predict(df_pos.drop(objective_function, axis=1))].shape[0]
    false_pos = df_pos[df_pos[objective_function] != model.predict(df_pos.drop(objective_function, axis=1))].shape[0]
    true_neg = df_neg[df_neg[objective_function] == model.predict(df_neg.drop(objective_function, axis=1))].shape[0]
    false_neg = df_neg[df_neg[objective_function] != model.predict(df_neg.drop(objective_function, axis=1))].shape[0]

    #print(true_pos)
    #conf_matrix = zip(['false_pos', 'true_pos', 'true_neg', 'false_neg'], [false_pos, true_pos, true_neg, false_neg])
    #conf_matrix = [] #pd.DataFrame(dtype=float)
    conf_matrix = [false_pos, true_pos, true_neg, false_neg]

    accuracy = (true_neg + true_pos) / (false_pos + true_pos + true_neg + false_neg)
    precision = true_pos / (false_pos + true_pos)
    sensitivity = true_pos / (true_pos + false_neg) #recall
    specificity = true_neg / (true_neg + false_pos)

    #conf_result = []
    conf_result = [accuracy, precision, sensitivity, specificity]

    #conf_result = [],
    #conf_result.append([accuracy, precision, sensitivity, specificity])
    return conf_matrix, conf_result


def ML(data, selected_features, objective_function): #, datapredict
    # print("selected_features in ML:", selected_features)
    # print("колонки:\n", data.columns)
    X = data[selected_features]
    y = data[objective_function]
    # print("Это Х:", X)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=11)
    N_train, _ = X_train.shape
    N_test, _ = X_test.shape
    results = []
    a=[]

    '''
    def ML3():
        model1 = LogisticRegression()
        model1.fit(X_train, y_train)
        err_train = np.mean(y_train != model1.predict(X_train))
        err_test = np.mean(y_test != model1.predict(X_test))
        probs = model1.predict_proba(X_test)
        a.append([y_test, probs])
        conf_matrix, conf_result = confusion(data[selected_features + [objective_function]], objective_function, model1)
        results.append([err_train, err_test, conf_matrix, conf_result])
        
    
    def ML1():
        # Определим важность признаков
        model0 = ExtraTreesClassifier()
        model0.fit(X, y)
        #print('ВВ')
        #print('Важность признаков: ', model0.feature_importances_)
        # X0_pr['ETreecl'] = model0.predict(X0)
        results.append(model0.feature_importances_)
    
    '''


    def ML3():
        gbr = xgb.XGBClassifier(learning_rate=1.5, max_depth=3, n_estimators=50, min_child_weight=10,
                                random_state=1)
        #rf = ensemble.GradientBoostingClassifier(learning_rate=1.1, max_depth=4, n_estimators=50)
            #RandomForestClassifier(n_estimators=200, random_state=11)
            #GradientBoostingClassifier(learning_rate=1.1,
            #                                     max_depth=4,
            #                                     n_estimators=50)
        gbr.fit(X_train, y_train)
        err_train = np.mean(y_train != gbr.predict(X_train))
        err_test = np.mean(y_test != gbr.predict(X_test))
        probs = gbr.predict_proba(X_test)
        a.append([y_test, probs])
        conf_matrix, conf_result = confusion(data[selected_features + [objective_function]], objective_function, gbr)
        results.append([err_train, err_test, conf_matrix, conf_result])
        #print('Ошибки rf ', err_train, err_test)
        #X0_pr['rf'] = rf.predict(X0)
        # print("Важности", gbr.feature_importances_)
        results.append(gbr.feature_importances_)


    '''
    def ML0():
        # МЕТОД БЛИЖАЙШИХ СОСЕДЕЙ
        knn = KNeighborsClassifier(n_neighbors=11)
        knn.fit(X_train, y_train)
        y_train_predict = knn.predict(X_train)
        y_test_predict = knn.predict(X_test)
        err_train = np.mean(y_train != y_train_predict)
        err_test = np.mean(y_test != y_test_predict)
        #print('Ошибки knn ', err_train, err_test)
        #X0_pr['knn'] = knn.predict(X0)

    def ML2():
        rfe = RFE(LogisticRegression(), 3)
        rfe = rfe.fit(X_train, y_train)
        # summarize the selection of the attributes
        err_train = np.mean(y_train != rfe.predict(X_train))
        err_test = np.mean(y_test != rfe.predict(X_test))
        #print('Ошибки rfe ', err_train, err_test)
        #X0_pr['rfe'] = rfe.predict(X0)

    def ML4():
        model2 = GaussianNB()
        model2.fit(X_train, y_train)
        err_train = np.mean(y_train != model2.predict(X_train))
        err_test = np.mean(y_test != model2.predict(X_test))
        #print('Ошибки model2 ', err_train, err_test)
        #X0_pr['NB'] = model2.predict(X0)

    def ML5():
        model3 = DecisionTreeClassifier()
        model3.fit(X_train, y_train)
        err_train = np.mean(y_train != model3.predict(X_train))
        err_test = np.mean(y_test != model3.predict(X_test))
        #print('Ошибки model3 ', err_train, err_test)
        #X0_pr['CART'] = model3.predict(X0)

    def ML6():
        rf = ensemble.RandomForestClassifier(n_estimators=200, random_state=11)
        rf.fit(X_train, y_train)
        err_train = np.mean(y_train != rf.predict(X_train))
        err_test = np.mean(y_test != rf.predict(X_test))
        #print('Ошибки rf ', err_train, err_test)
        #X0_pr['rf'] = rf.predict(X0)

    def ML7():
        model4 = SVC()
        model4.fit(X_train, y_train)
        err_train = np.mean(y_train != model4.predict(X_train))
        err_test = np.mean(y_test != model4.predict(X_test))
        #print('Ошибки SVC ', err_train, err_test)
        #X0_pr['SVC'] = model4.predict(X0)

    def ML8():
        alphas = np.array([1, 0.1, 0.01, 0.001, 0.0001, 0])
        grid = GridSearchCV(estimator=Ridge(), param_grid=dict(alpha=alphas))
        grid.fit(X_train, y_train)
        #print('grid ', grid)
        #print('gridbs', grid.best_score_)
        #print('gridbea', grid.best_estimator_.alpha)
        err_train = np.mean(y_train != grid.predict(X_train))
        err_test = np.mean(y_test != grid.predict(X_test))
        #print('Ошибки GridsearchCV ', err_train, err_test)
        #X0_pr['GridsearchCV'] = grid.predict(X0)

    def ML9():
        n_neighbors_array = [1, 2, 3, 4, 5, 6, 7, 10, 15]
        grid0 = GridSearchCV(KNeighborsClassifier(), param_grid={'n_neighbors': n_neighbors_array})
        grid0.fit(X,y)#_train, y_train)  # вообще нужно X_train,y_train, но данных слишком мало
        best_cv_err = 1 - grid0.best_score_
        best_n_neighbors = grid0.best_estimator_.n_neighbors
        #print('best_cv_err, best_n_neighbors - ', best_cv_err, best_n_neighbors)

    def ML10():
        dtree = DecisionTreeRegressor().fit(X_train, y_train)
        err_train = np.mean(y_train != dtree.predict(X_train))
        err_test = np.mean(y_test != dtree.predict(X_test))
        #print('Ошибки DTR ', err_train, err_test)
        #X0_pr['DTR'] = dtree.predict(X0)

    def ML11():
        bdt = BaggingRegressor(DecisionTreeRegressor()).fit(X_train, y_train)
        err_train = np.mean(y_train != bdt.predict(X_train))
        err_test = np.mean(y_test != bdt.predict(X_test))
        #print('Ошибки bdt ', err_train, err_test)
        #X0_pr['bdt'] = bdt.predict(X0)

    def ML12():
        b_dtree = BaggingClassifier(DecisionTreeClassifier(), n_estimators=300, random_state=42)
        b_dtree.fit(X_train, y_train)
        err_train = np.mean(y_train != b_dtree.predict(X_train))
        err_test = np.mean(y_test != b_dtree.predict(X_test))
        #print('Ошибки b_dtree ', err_train, err_test)
        #X0_pr['b_dtree'] = b_dtree.predict(X0)

    def ML13():
        svm0 = svm.SVR()
        svm0.fit(X_train, y_train)
        err_train = np.mean(y_train != svm0.predict(X_train))
        err_test = np.mean(y_test != svm0.predict(X_test))
        #print('Ошибки SVR ', err_train, err_test)
        #X0_pr['SVR'] = svm0.predict(X0)
        
        
    ML0()
    ML1()
    ML2()
    ML3()
    ML4()
    ML5()
    ML6()
    ML7()
    #ML8()# RuntimeWarning
    #ML9()
    ML10()
    ML11()
    ML12()
    ML13()
    '''

    ML3()
    #ML1()
    results.append(a)


    return results





