import pandas as pd
from dash.dependencies import Input, Output

import data_frames as dfs
from dash_app import app
from index_html import generate_heat_gradients
pd.set_option('mode.chained_assignment', None)


@app.callback(
    Output('dt-predictors-imp', 'selected_rows'),
    [
        Input('dt-predictors-imp', 'derived_virtual_data'),
    ])
def select_rows(data):
    """Выбор строк из обратного вызова не работает."""
    if not data:
        return
    current_df = pd.DataFrame(data)
    select_rows_nums = []
    for i in current_df.index:
        importance = current_df["Важность"][i]
        if importance > dfs.THRESHOLD:
            select_rows_nums.append(i)
    response = select_rows_nums
    return response


# '''

@app.callback(
    Output('dt-predictors-imp', 'data'),
    [Input('predictors-button-show', 'n_clicks'), ]
)
def update_features_importance(button_clicks):
    if button_clicks is not None:
        result = dfs.run_learning(dfs.main_df, list(dfs.main_table_df["ИмяКолонки"].values), "nedads_flg")
        importance_list = result[1]
        for i in dfs.main_table_df.index:
            dfs.main_table_df["Важность"].loc[i] = importance_list[i]

        main_table_df = dfs.main_table_df.sort_values(by="Важность", ascending=False)
        return main_table_df.to_dict("rows")


@app.callback(
    Output('dt-predictors-imp', 'style_data_conditional'),
    [Input('dt-predictors-imp', 'derived_virtual_data')]
)
def update_color_pred(data):
    if not data:
        return

    styles_list = [{'if': {'column_id': 'Важность'},
                    'columnWidth': "150px", 'maxWidth': '150px'
                    }, ]
    df = pd.DataFrame(data)
    styles_list.extend(generate_heat_gradients(df))
    return styles_list
