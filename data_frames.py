import os
import warnings

import pandas as pd

from simple_analysis import ML

warnings.filterwarnings("ignore", category=FutureWarning)

WORKING_DIRECTORY = 'resources'
FILE_NAME = 'egrul_ads.csv'
THRESHOLD = 0.023



main_df = pd.read_csv(os.path.join(WORKING_DIRECTORY, FILE_NAME), sep=',')
cols_df = pd.read_csv(os.path.join(WORKING_DIRECTORY, 'cols.csv'), sep=';')

global_real_all_pred_tracers_numbers = []
res_for_graph_conf_matrix = None
res_for_graph_conf_result = None
target_objective_function = ['none']

# Список всех колонок. Далее сделать all_columns_in_main_df=[],all_columns_in_main_df.append(...),all_columns_in_main_df[-1]
all_columns_in_main_df = main_df.columns.values.copy()
total_columns_in_main_df = len(all_columns_in_main_df)


for_predictors_data_columns_df = cols_df.loc[cols_df["status"] == 0]
for_choices_data_df = cols_df.loc[cols_df["status"] == 1]
three_df = None  # Третий набор, пока не знаю где использоваться будет.


def create_predictorc_data_frame(data_df: "pd.DataFrame", columns_info_df: "pd.DataFrame"):

    # может нехватать колонок, чтобы не было ошибки делаю пересечение
    columns = set(data_df.columns) & set(columns_info_df["name"].values)

    data = {
        "Важность": [],
        "Признаки": [],
        "ИмяКолонки": [],  # фактически ид колонки, чтобы можно было верно подставить важность.
    }
    for column in columns:
        cell_index = cols_df.loc[cols_df["name"] == column].index
        data["Важность"].append(0)  # Пока важность ещё не определена
        data["Признаки"].append(cols_df["decr"][cell_index])
        data["ИмяКолонки"].append(column)

    data_frame = pd.DataFrame(data=data, columns=data.keys())
    return data_frame


main_table_df = create_predictorc_data_frame(main_df, for_predictors_data_columns_df)


def run_learning(main_data_frame, columns, objective_function):
    """Сделал эту функцию, чтобы была возможность задать глобалки, в датафреймах"""
    result = ML(main_data_frame, columns, objective_function)

    importance_list = result[1]  # список важностей

    global res_for_graph_conf_matrix
    global res_for_graph_conf_result

    res_for_graph_conf_matrix = result[0][2]  # фиг знает, что это
    res_for_graph_conf_result = result[0][3]
    return result
