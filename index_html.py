import dash_core_components as dcc
import dash_html_components as html
import dash_table
import pandas as pd

import data_frames as dfs
from dash_app import app


def generate_heat_gradients(data_frame: "pd.DataFrame" = None):
    values = list(map(float, data_frame["Важность"].values))
    max_value = max(values)

    if max_value:
        step = 255 / max_value
    else:
        step = 0

    styles = []
    for i in data_frame.index:
        value = values[i]
        color_intensity = 255 - value * step
        style = {
            "if": {"row_index": i},
            "backgroundColor": "rgb(255," + str(int(color_intensity)) + ", " + "255" + ")"
        }
        styles.append(style)
    return styles


app.layout = html.Div(id="base_container", children=[
    html.H2(children='Прогнозирование нахождение Юр.лица по указанному '
                     'адресу для целей Упрвления Регистрации и Учета Налогоплательщиков'),
    html.Div(id='div-objective-function', children=[
        html.Label('Выбрать цель моделирования'),
        dcc.Dropdown(
            options=[{'label': dfs.for_choices_data_df["decr"][i], 'value': i} for i in dfs.for_choices_data_df.index],
            id='objective-function',
            value='None'
        ),
        html.Button(id='objective-function-button', children='Применить выбранную цулевую функцию'),
        html.Div('Описание')
    ]),
    html.Div(id='choose1-objective-function', children=[
        html.H3([
            html.Div(id='output-objective-function',
                     children=['Цель моделирования: ' + str(dfs.target_objective_function[-1])]
                     ),
        ]),
        html.Div(id='', children=[
            html.Div(id='Pred2', children=[
                html.Label('Предикторы:'),
                dash_table.DataTable(
                    data=dfs.main_table_df.to_dict("rows"),
                    columns=[{"name": i, "id": i} for i in ['Важность', 'Признаки']],  # cols_list[-1].columns
                    id='dt-predictors-imp',
                    style_table={'overflowX': 'scroll', 'textAlign': 'center'},
                    style_cell={
                        'textAlign': 'left',
                        'width': '400px',
                        'whiteSpace': 'normal'
                    },
                    n_fixed_rows=1,
                    # filtering=True,
                    # sorting=True,
                    # sorting_type="single",
                    row_selectable="multi",
                    selected_rows=[0, 1, 2, ],
                    style_header={
                        'fontWeight': 'bold'
                    },
                    style_data_conditional=[{
                        'if': {'column_id': 'Важность'},
                            'columnWidth': "150px", 'maxWidth': '150px'
                    }]
                )
            ], style={'width': '60%', 'display': 'inline-block'}),
            html.Div(
                id="button_container_for_predictors_table",
                children=[
                    html.Button(id='predictors-button-show', children='Определить важность признаков',
                                style={"background-color": "gainsboro"}),
                    html.Br(),
                    html.Button(id='create-model', type='submit', children='Создать модель',
                                style={"background-color": "gainsboro"}),
                ],

            ),

        ]),
        html.Div(id='result', children=[
            html.Div(id='div-result-model'),
            html.Label('ROC-кривая'),
            html.Div(id='graphs'),
        ]),
    ]),
])
