import numpy as np
import pandas as pd
import dash_html_components as html
import dash_core_components as dcc
import plotly.graph_objs as go


def generate_table(df, max_rows=10):
    return html.Table(
        [html.Tr([html.Th(col) for col in df.columns])] +
        [html.Tr([
            html.Td(df.iloc[i][col]) for col in df.columns
        ]) for i in range(min(len(df), max_rows))]
    )


def cols_list_add(df):
    cols = pd.DataFrame(df.columns.values, columns=['label'])
    cols['value'] = [0 for i in range(df.shape[1])]
    return cols


#Slider
def feature_slider(df, id, feature):
    return dcc.Slider(id=id,
                      min=df[feature].min(),
                      max=df[feature].max(),
                      value=df[feature].min(),
                      marks={str(val): str(val) for val in df[feature].unique()})


def graph(xaxis_column, yaxis_column, text_marks, xaxis_column_name, xaxis_type, yaxis_column_name, yaxis_type):#, multi=1

    df = pd.read_csv('resources/egrul_ads.csv', sep=',')
    '''
    def multi_graph(x1, x2, y1, y2):
        fig = tls.make_subplots(rows=2, cols=1, shared_xaxes=True, vertical_spacing=0.009, horizontal_spacing=0.009)
        fig['layout']['margin'] = {'l': 30, 'r': 10, 'b': 50, 't': 25}

        fig.append_trace({'x': x1, 'y': y1, 'type': 'scatter', 'name': 'dopokved_cls_avgcnt'}, 1, 1)
        fig.append_trace({'x': x2, 'y': y2, 'type': 'scatter', 'name': 'dolzhn_cnt'}, 2, 1)
        #fig.append_trace({'x': main_df.Time, 'y': main_df.Volume, 'type': 'bar', 'name': 'Volume'}, 2, 1)
    '''

    #def simple_graph():
        #return

    return {
            'data': [go.Scatter(
                x=xaxis_column,
                y=yaxis_column,
                text=text_marks,
                mode='lines',
                marker={
                    'size': 15,
                    'opacity': 0.5,
                    'line': {'width': 0.5, 'color': 'white'}
                }
            )],
            'layout': go.Layout(
                xaxis={
                    'title': xaxis_column_name,
                    'type': 'linear' if xaxis_type == 'Linear' else 'log'
                },
                yaxis={
                    'title': yaxis_column_name,
                    'type': 'linear' if yaxis_type == 'Linear' else 'log'
                },
                margin={'l': 40, 'b': 40, 't': 10, 'r': 0},
                hovermode='closest'
            )
        }
    # if multi == 1 else multi_graph(main_df['inn_id'].index, main_df['inn_id'].index, main_df['dopokved_cls_avgcnt'], main_df['dolzhn_cnt'])


def simple_func1(ff, numparts):
    ffsort = np.sort(ff)
    mnsort = ffsort[1]
    mxsort = ffsort[ffsort.shape[0] - 1]
    mn = ff.min()
    mx = ff.max()
    if numparts == 4:
        mn = mnsort
        mx = mxsort
    ff2 = []
    ffmean = []
    l = (mx - mn) / numparts
    ppp = mn
    for i in range(numparts):
        if (ppp + l < mx - 0.0001):
            ffmeanpredv = ff[(ff >= ppp) & (ff < ppp + l)]
        else:
            ffmeanpredv = ff[(ff >= ppp)]
        ff2.append(ffmeanpredv.shape[0])
        if ffmeanpredv.shape[0] != 0:
            ffmean.append(ffmeanpredv.mean())
        else:
            ffmean.append(0)
        ppp += l
    ff2 = pd.DataFrame(ff2)
    ff2.columns = ['Chastota']
    ff2['SredZnach'] = pd.DataFrame(ffmean)
    ff2['OtnosChast'] = ff2['Chastota'] / ff2['Chastota'].sum()
    ff2OC = []
    ff2Plotnost = []
    q = 0
    for i in range(ff2.shape[0]):
        q += ff2['OtnosChast'][i]
        ff2Plotnost.append(ff2['OtnosChast'][i] / l)
        ff2OC.append(q)
    ff2['FRaspred'] = pd.DataFrame(ff2OC)
    ff2['Plotnost'] = pd.DataFrame(ff2Plotnost)
    return ff2


def update_cols(df, status_columns, df2):

    def rename_decr(i, num):
        if df2['status'][num] == 1:
            status_columns[1][i] = 1
            status_columns[3][i] = 0
            status_columns[2][i] = df2['decr'][num]
        else:
            status_columns[1][i] = 0
            status_columns[2][i] = df2['decr'][num]

    # df2.columns = ['ind', 'name', 'decr', 'status']
    df2 = pd.read_csv(df2, sep=';')
    num = 0
    for i in df.columns:
        for j in range(df2.shape[0]):
            if i == df2['name'][j]:
                rename_decr(num, j)
        num += 1

